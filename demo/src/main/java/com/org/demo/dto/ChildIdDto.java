package com.org.demo.dto;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChildIdDto {
	private Long childId;
}
