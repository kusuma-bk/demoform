package com.org.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ParentChildRequestDto {
	private long parentId;
	private long childId;

}
