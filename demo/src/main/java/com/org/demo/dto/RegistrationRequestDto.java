package com.org.demo.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegistrationRequestDto {
	@NotEmpty(message = "User Name should not be empty")
	private String userName;

	@NotEmpty(message = "Password should not be empty")
	@Size(min = 8, max = 10, message = "Password should be size of 8 length minimum")
	private String password;

	@NotEmpty(message = "Phone Number should not be empty")
	// @Pattern(regexp = "(^$|[0-9a-zA-Z]{10})", message = "phone number must be 10
	// digits special characters not allowed")
	@Pattern(regexp = "(^$|[0-9]{10})", message = "phone number must be  10 digits special characters not allowed")
	private String phoneNumber;

	@Email
	@Pattern(regexp = ".+@.+\\..+", message = "Please provide a valid email address")
	private String emailId;

	private String parentReferenceId;

	// "[-~]*$" allows alphanumeric and spl character
	// @Pattern(regexp =
	// "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\\p{Punct})[\\p{ASCII}&&[\\S]]{8,}$",
	// message = " reference length must be 8")
	// private String referenceId;

}
