package com.org.demo.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ParentChildTreeResponseDto {

	private long parentId;
	private List<ChildIdDto> childId;

}
