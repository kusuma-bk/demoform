package com.org.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegistrationResponseDto {

	private String message;
	private int statusCode;
	private String referenceId;

}
