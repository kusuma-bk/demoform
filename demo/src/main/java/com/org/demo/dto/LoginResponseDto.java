package com.org.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResponseDto {

	private String message;
	private int statusCode;
	private long userId;

}
