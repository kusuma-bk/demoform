package com.org.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long userId;
	
	@Size(max = 50)
	private String userName;
	@Size(max = 50)
	private String password;
	@Size(max = 20)
	private String phoneNumber;
	@Size(max = 50)
	private String emailId;
	@Size(max = 50)
	private String referenceId;
	@Size(max = 15)
	private String status;
	
	
	

}
