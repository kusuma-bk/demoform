package com.org.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.demo.entity.Reference;

@Repository
public interface ReferenceRepository extends JpaRepository<Reference, Long> {

	int countByParentId(long parentId);

	Optional<List<Reference>> findByParentId(long parentId);

	@Query(value = " SELECT * FROM reference WHERE parent_id IN (SELECT child_id FROM reference)", nativeQuery = true)
	Optional<List<Reference>> findChildIdAsParent(long userId);

	Optional<List<Reference>> findByParentIdIn(List<Long> parentIds);
}