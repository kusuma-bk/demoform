package com.org.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.demo.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByEmailIdOrPhoneNumber(String emailId, String phoneNumber);

	@Query(value = "SELECT * FROM user WHERE ?1 IN(email_id, phone_number)and password = ?2", nativeQuery = true)
	Optional<User> loginCheck(String userName, String password);

	Optional<User> findByReferenceId(String referenceId);

}
