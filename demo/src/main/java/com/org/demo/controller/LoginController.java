package com.org.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.demo.dto.LoginRequestDto;
import com.org.demo.dto.LoginResponseDto;
import com.org.demo.exception.UserException;
import com.org.demo.service.LoginService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("login")
public class LoginController {

	@Autowired
	LoginService loginService;

	@PostMapping
	ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequestDto loginRequestDto ) throws UserException {
		return new ResponseEntity<>(loginService.login(loginRequestDto), HttpStatus.OK);
	}
}
