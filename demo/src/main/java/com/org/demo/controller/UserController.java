package com.org.demo.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.demo.dto.ParentChildTreeResponseDto;
import com.org.demo.dto.RegistrationRequestDto;
import com.org.demo.dto.RegistrationResponseDto;
import com.org.demo.exception.UserException;
import com.org.demo.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping
	ResponseEntity<RegistrationResponseDto> register(@Valid @RequestBody RegistrationRequestDto registrationRequestDto)
			throws UserException {
		return new ResponseEntity<>(userService.register(registrationRequestDto), HttpStatus.CREATED);
	}

	@GetMapping("/{userId}")
	ResponseEntity<List<ParentChildTreeResponseDto>> parentChildTree(@PathVariable("userId") long userId)
			throws UserException {
		//userService.sampleRecurssion(Collections.singletonList(userId));
		return new ResponseEntity<>(userService.parentChildTree(userId), HttpStatus.OK);
	}

}
