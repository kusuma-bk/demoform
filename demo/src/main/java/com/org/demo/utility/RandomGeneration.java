package com.org.demo.utility;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.org.demo.entity.User;
import com.org.demo.exception.UserException;
import com.org.demo.repository.UserRepository;

@Configuration
public class RandomGeneration {
	@Autowired
	UserRepository userRepository;
	
	Random random;

	public String randomGenerationValue() {
		random = new Random();

		int leftLimit = 48; // numeral '0'
		int rightLimit = 122; // letter 'z'
		int targetStringLength = 6;

		String referenceId = random.ints(leftLimit, rightLimit + 1)
				.filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(targetStringLength)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		Optional<User> optionalReferenceId = userRepository.findByReferenceId(referenceId);
		if (optionalReferenceId.isPresent()) {
			randomGenerationValue();
		}
		
		return referenceId;
	}

}
