package com.org.demo.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.org.demo.entity.Reference;
import com.org.demo.exception.UserException;
import com.org.demo.repository.ReferenceRepository;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class ParentChild {

	@Autowired
	ReferenceRepository referenceRepository;

	// public String parentChild(ParentChildRequestDto parentChildRequestDto) {
	public String parentChild(long parentId, long childId) throws UserException {

		int numberOfChild = referenceRepository.countByParentId(parentId);
		log.info("numberOfChild {} = ", numberOfChild);
		Reference reference = new Reference();
		// BeanUtils.copyProperties(parentChildRequestDto, reference);
		reference.setParentId(parentId);
		reference.setChildId(childId);
		if (numberOfChild >= 2) {
			throw new UserException("Parent can not have more than 2 child");
		}

		referenceRepository.save(reference);

		return "inserted successfully";
	}

}
