package com.org.demo.service;

import java.util.List;

import com.org.demo.dto.ParentChildTreeResponseDto;
import com.org.demo.dto.RegistrationRequestDto;
import com.org.demo.dto.RegistrationResponseDto;
import com.org.demo.exception.UserException;

public interface UserService {

	public RegistrationResponseDto register(RegistrationRequestDto registrationRequestDto) throws UserException;

	public List<ParentChildTreeResponseDto> parentChildTree(long userId) throws UserException;
	
	public void sampleRecurssion(List<Long> userIds);

}
