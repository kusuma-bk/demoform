package com.org.demo.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.demo.dto.ChildIdDto;
import com.org.demo.dto.ParentChildTreeResponseDto;
import com.org.demo.dto.RegistrationRequestDto;
import com.org.demo.dto.RegistrationResponseDto;
import com.org.demo.entity.Reference;
import com.org.demo.entity.User;
import com.org.demo.exception.UserException;
import com.org.demo.repository.ReferenceRepository;
import com.org.demo.repository.UserRepository;
import com.org.demo.utility.Constants;
import com.org.demo.utility.ParentChild;
import com.org.demo.utility.RandomGeneration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RandomGeneration random;

	@Autowired
	ReferenceRepository referenceRepository;

	@Autowired
	ParentChild parentChild;

	@Autowired
	UserService userService;
	Map<Long, List<Reference>> childGlobal = new HashMap<>();

	@Override
	public RegistrationResponseDto register(RegistrationRequestDto registrationRequestDto) throws UserException {
		Optional<User> optionalParentReferenceId = userRepository
				.findByReferenceId(registrationRequestDto.getParentReferenceId());
		if (!optionalParentReferenceId.isPresent()) {
			throw new UserException("Reference Code does not exists");
		}
		User user = new User();

		Optional<User> optionalUser = userRepository.findByEmailIdOrPhoneNumber(registrationRequestDto.getEmailId(),
				registrationRequestDto.getPhoneNumber());
		if (optionalUser.isPresent()) {
			throw new UserException(Constants.USER_EXISTS_ERROR);
		}
		BeanUtils.copyProperties(registrationRequestDto, user);
		user.setReferenceId(random.randomGenerationValue());
		user = userRepository.save(user);
		log.info("user details {} = ", user);
		RegistrationResponseDto registrationResponseDto = new RegistrationResponseDto();
		registrationResponseDto.setMessage(Constants.USER_REGISTERED_SUCCESS);
		registrationResponseDto.setStatusCode(Constants.USER_REGISTERED_SUCCESS_STATUS_CODE);
		registrationResponseDto.setReferenceId(random.randomGenerationValue());

		/*
		 * ParentChildRequestDto parentChildRequestDto = new ParentChildRequestDto();
		 * parentChildRequestDto.setParentId(optionalParentReferenceId.get().getUserId()
		 * ); parentChildRequestDto.setChildId(user.getUserId());
		 * parentChild.parentChild(parentChildRequestDto);
		 */
		parentChild.parentChild(optionalParentReferenceId.get().getUserId(), user.getUserId());

		return registrationResponseDto;
	}

	List<ParentChildTreeResponseDto> listOfParentChildTreeResponseDto;

	
	private List<ParentChildTreeResponseDto> parentChildTreeResponseDtos = new ArrayList<>();
	List<Reference> parentReferenceInfo = new ArrayList<>();

	@Override
	public List<ParentChildTreeResponseDto> parentChildTree(long userId) throws UserException {
       userService.sampleRecurssion(Collections.singletonList(userId));
       
      Iterator<Entry<Long, List<Reference>>> itr = childGlobal.entrySet().iterator();
      
      while(itr.hasNext()) {
    	  ParentChildTreeResponseDto parentChildTreeResponseDto=new ParentChildTreeResponseDto();
          parentChildTreeResponseDto.setParentId(itr.next().getKey());
    	  
          List<ChildIdDto> childIdDtos=new ArrayList<>();
    	  itr.next().getValue().stream().forEach(refe->{
    		  ChildIdDto childDto = new ChildIdDto();
    		  childDto.setChildId(refe.getChildId());
    		  childIdDtos.add(childDto);
    	  });
      parentChildTreeResponseDto.setChildId(childIdDtos);
      }
      
      
     
       
		return parentChildTreeResponseDtos;
	}


	@Override
	public void sampleRecurssion(List<Long> userIds) {
		log.info("in sample Recusrrsion");
		ParentChildTreeResponseDto childTreeResponseDto = new ParentChildTreeResponseDto();
		/*
		 * for (Long userId : userIds) { ChildIdDto childDto = new ChildIdDto();
		 * Optional<List<Reference>> sample =
		 * referenceRepository.findByParentId(userId);
		 * 
		 * }
		 */

//		UserServiceImpl userServiceImpl = new UserServiceImpl();
		if (!userIds.isEmpty()) {
			Optional<List<Reference>> userChailds = referenceRepository.findByParentIdIn(userIds);
			log.info("userChailds = {}", userChailds);
			if (userChailds.isPresent()) {

				Map<Long, List<Reference>> child = userChailds.get().stream()
						.collect(Collectors.groupingBy(Reference::getParentId));
				childGlobal.putAll(child);

				List<Long> chilsParents = userChailds.get().stream().map(Reference::getChildId)
						.collect(Collectors.toList());
				if (!chilsParents.isEmpty()) {
					userService.sampleRecurssion(chilsParents);
				}

			}
		}
		log.info("childGlobal = {}", childGlobal);
		/*
		 * List<Reference> childsssss =
		 * childGlobal.values().stream().collect(Collectors.toList()).stream()
		 * .flatMap(val -> val.stream()).collect(Collectors.toList());
		 * log.info("global list childsssss= {}", childsssss);
		 */
	}

	

}
