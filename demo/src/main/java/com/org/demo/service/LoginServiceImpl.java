package com.org.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.demo.dto.LoginRequestDto;
import com.org.demo.dto.LoginResponseDto;
import com.org.demo.entity.User;
import com.org.demo.exception.UserException;
import com.org.demo.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserRepository userRepository;

	@Override
	public LoginResponseDto login(LoginRequestDto loginRequestDto) throws UserException {
		log.info("saiiiiiiiiii");
		Optional<User> optionalUser = userRepository.loginCheck(loginRequestDto.getUserName(),
				loginRequestDto.getPassword());
		log.info("details= {}", optionalUser);
		if (!optionalUser.isPresent()) {
			throw new UserException("Invalid credentials");
		}

		LoginResponseDto loginResponseDto = new LoginResponseDto();
		loginResponseDto.setMessage("Logged in successfully!!");
		loginResponseDto.setStatusCode(702);
		loginResponseDto.setUserId(optionalUser.get().getUserId());

		return loginResponseDto;
	}

}
