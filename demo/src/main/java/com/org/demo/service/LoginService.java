package com.org.demo.service;

import com.org.demo.dto.LoginRequestDto;
import com.org.demo.dto.LoginResponseDto;
import com.org.demo.exception.UserException;

public interface LoginService {

	public LoginResponseDto login(LoginRequestDto loginRequestDto) throws UserException;
}
